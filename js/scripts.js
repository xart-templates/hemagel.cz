jQuery.noConflict();
jQuery(document).ready(function($){

	$('html').addClass('js');

	$('.box_slideshow').each(function(){
		var i = $('.item',this).length;
		if(i>1){
			$(this).append('<a class="prev">&lt;</a><a class="next">&gt;</a><div class="pager"/>');
			$('.items',this).cycle({
				slides: '.item',
				fx: 'scrollHorz',
				speed: 600,
				timeout: 5000,
				pauseOnHover: true,
				swipe: true,
				prev: $('.prev',this),
				next: $('.next',this),
				pager: $('.pager',this),
			});
		}
	});
	$(document.documentElement).keyup(function(event){
		if (event.keyCode == 37) {
			$('.box_slideshow .items').cycle('prev');
		} else if (event.keyCode == 39) {
			$('.box_slideshow .items').cycle('next')
		}
	});

	$('.box_slider').each(function(){
		$(this).append('<a class="prev">&lt;</a><a class="next">&gt;</a>');
		$('.items',this).cycle({
			slides: '.item',
			fx: 'carousel',
			speed: 200,
			timeout: 8000,
			pauseOnHover: true,
			swipe: true,
			prev: $('.prev',this),
			next: $('.next',this),
		});
	});

	$('.eshop_category .item a').matchHeight({
		property: 'min-height',
	});

	// placeholder
	if (document.createElement('input').placeholder==undefined){
		$('[placeholder]').focus(function(){
			var input=$(this);
			if(input.val()==input.attr('placeholder')){
				input.val('');
				input.removeClass('placeholder')
			}
		}).blur(function(){
			var input=$(this);
			if(input.val()==''||input.val()==input.attr('placeholder')){
				input.addClass('placeholder');
				input.val(input.attr('placeholder'))
			}
		}).blur()
	}

	// iCheck
	$('input[type="checkbox"],input[type="radio"]').iCheck();

	// selectBoxIt
	$('select:not([multiple])').selectBoxIt();

});
